/*
 * Children.h
 *
 *  Created on: 2017-06-13
 *      Author: Patrick Fournier
 */

#pragma once

#include <array>
#include <map>

#include "Types.h"

// Forward declaration of CoalTree.
class CoalTree;

class Children : protected Types<CoalTree> {
public:
    /***** Types *****/
    typedef typename Types<CoalTree>::nodes_pair nodes_pair;
    typedef typename Types<CoalTree>::nodesPairs_map nodesPairs_map;

    typedef std::array<CoalTree*, 2> children;

    /***** Constructors *****/
    Children(size_t pos, CoalTree* left, CoalTree* right)
        : _nodesPairs{std::make_pair(pos, nodes_pair{left, right})} {};

    Children(size_t pos, nodes_pair children)
        : _nodesPairs{std::make_pair(pos, children)} {};

    Children(Children&& children)
        : _nodesPairs{} {
        _nodesPairs.swap(children._nodesPairs);
    };

    Children() = default;

    Children(const Children&) = default;

    /***** Modifying methods *****/
    void insert_or_assign(size_t pos, nodes_pair children) {
        _nodesPairs.insert_or_assign(pos, children);
    };

    void substitute(size_t pos, CoalTree* oldNode, CoalTree* newNode) {
        for (CoalTree*& node : operator[](pos)) {
            if (node == oldNode) {
                node = newNode;
                break;
            }
        }
    };

    Children& operator=(Children&& rhs) {
        if (&rhs != this) {
            _nodesPairs.clear();
            _nodesPairs.swap(rhs._nodesPairs);
        }

        return *this;
    };

    /***** Iteration *****/
    nodes_pair& operator[](size_t pos) {
        return _nodesPairs.lower_bound(pos)->second;
    };

    const nodes_pair& operator[](size_t pos) const {
        return _nodesPairs.lower_bound(pos)->second;
    };

    typename nodesPairs_map::iterator begin() { return _nodesPairs.begin(); };
    const typename nodesPairs_map::const_iterator begin() const {
        return _nodesPairs.begin();
    };

    typename nodesPairs_map::iterator end() { return _nodesPairs.end(); };
    const typename nodesPairs_map::const_iterator end() const {
        return _nodesPairs.end();
    };

    /***** Informations *****/
    size_t size() const { return _nodesPairs.size(); };

protected:
    /***** Protected attributes *****/
    nodesPairs_map _nodesPairs;
};
