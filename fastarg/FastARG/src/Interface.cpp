/*
 * Interface.cpp
 *
 *  Created on: 2017-06-26
 *      Author: Patrick Fournier
 */

#pragma omp once

#include <random>
#include <vector>

#include <Rcpp.h>

#ifdef _OPENMP
#include <omp.h>
#endif

#include "MoonShine.h"
#include "SequentialSnipSequence.h"

// [[Rcpp::export]]
Rcpp::NumericMatrix compute(size_t n,
                            Rcpp::LogicalMatrix genotypes,
                            Rcpp::LogicalVector phenotypes,
                            Rcpp::List parameters,
                            int rng_seed,
                            size_t nbCores) {
    using namespace Rcpp;
    using std::move;
    using std::transform;

    /* Wraps Rcpp objects */
    // Leaves.
    std::vector<SequentialSnipSequence> leaves_;
    for (int i{0}; i < genotypes.nrow(); ++i) {
        std::string str;
        for (int j{0}; j < genotypes.ncol(); ++j)
            str += genotypes(i, j) ? "1" : "0";
        leaves_.emplace_back(SequentialSnipSequence{str});
    }

    Parameters parameters_;

    for (const auto& phenotype : phenotypes)
        parameters_.phenotypes.emplace_back(phenotype);

    IntegerVector locusCoordinates{as<IntegerVector>(parameters[0])};
    parameters_.locusCoordinates
        = {locusCoordinates.begin(), locusCoordinates.end()};
    NumericVector penetrance{as<NumericVector>(parameters[1])};
    parameters_.penetrance = {penetrance.begin(), penetrance.end()};

    parameters_.sequencesSize     = genotypes.ncol();
    parameters_.numberOfSequences = genotypes.nrow();

    parameters_.mutationRate            = parameters[2];
    parameters_.recombinationRate       = parameters[3];
    parameters_.populationEffectiveSize = parameters[4];

    NumericMatrix output{static_cast<int>(n), genotypes.ncol()};

#pragma omp parallel for num_threads(nbCores) schedule(static) \
    shared(output, leaves_, parameters_)
    for (size_t k = 0; k < n; ++k) {
        std::mt19937 eng(rng_seed + rng_seed * omp_get_thread_num());

        MoonShine arg{parameters_, leaves_};
        arg.run(eng);

        std::vector<long double> log_prob_pheno{arg.probs_pheno()};

        // transform(log_prob_pheno.begin(),
        //           log_prob_pheno.end(),
        //           log_prob_pheno.begin(),
        //           [&arg](long double x) -> long double {
        //               return arg.weight() + x;
        //           });

#pragma omp critical
        move(
            log_prob_pheno.begin(), log_prob_pheno.end(), output(k, _).begin());
    }

    return output;
}
