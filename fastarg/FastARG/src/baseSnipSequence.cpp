/*
 * baseSnipSequence.h
 *
 *  Created on: 2018-03-15
 *      Author: Éric Marcotte & Patrick Fournier
 *
 *  This class contains attributes and methods shared among every SnipSequence
 *  classes.
 */

#include "baseSnipSequence.h"

using std::slice;

baseSnipSequence::baseSnipSequence(string aSnipDataSequence) {
    assert(aSnipDataSequence.size() > 0);

    // Initialize the size and the number of block for the sequence.
    this->sizeOfSequence = aSnipDataSequence.size();
    this->numberOfBlock  = sizeOfSequence / BIT_SIZE_OF_DATA_TYPE;
    if (sizeOfSequence % BIT_SIZE_OF_DATA_TYPE != 0) { ++this->numberOfBlock; }

    // Converts the string of 1 and 0 into the binary encoding.
    this->snipsData
        = baseSnipSequence::convertStringToFastSequence(aSnipDataSequence);
}

baseSnipSequence::baseSnipSequence(valarray<DATA_TYPE> snipsDataSequence,
                                   size_t sizeOfSequence) {
    assert(snipsDataSequence.size() > 0);

    this->sizeOfSequence = sizeOfSequence;
    this->numberOfBlock  = sizeOfSequence / BIT_SIZE_OF_DATA_TYPE;

    if (sizeOfSequence % BIT_SIZE_OF_DATA_TYPE != 0) { ++this->numberOfBlock; }
    assert(this->numberOfBlock == snipsDataSequence.size());

    this->snipsData = snipsDataSequence;
}

std::valarray<DATA_TYPE> baseSnipSequence::convertStringToFastSequence(
    string aString) {
    assert(aString.size() > 0);

    // Computes the number of blocks needed.
    size_t numberOfBlock = aString.size() / BIT_SIZE_OF_DATA_TYPE;
    if (aString.size() % BIT_SIZE_OF_DATA_TYPE != 0) { ++numberOfBlock; }

    // Initialize the array with the right size.
    std::valarray<DATA_TYPE> resultVector(numberOfBlock);

    // Temporary variable for each block.
    DATA_TYPE number       = 0;
    DATA_TYPE bitIndicator = BIT_INDICATOR;
    string tempString      = "";

    for (size_t i = 0; i < numberOfBlock; ++i) {
        // We extract the right segment of the sequence
        tempString
            = aString.substr(i * BIT_SIZE_OF_DATA_TYPE, BIT_SIZE_OF_DATA_TYPE);
        // If it is the last block we maybe need to fill it with zeroes.
        if (i == numberOfBlock - 1) {
            tempString.append(BIT_SIZE_OF_DATA_TYPE - tempString.size(), '0');
        }
        for (size_t j = 0; j < BIT_SIZE_OF_DATA_TYPE; ++j) {
            // If we have a 1 we switch the right bit to 1. |= is the bitwise
            // or.
            if (tempString.at(j) == '1') { number |= bitIndicator; }
            else if (tempString.at(j) != '0') {
                throw std::runtime_error(
                    string(__FILE__) + ":" + std::to_string(__LINE__) + ": "
                    + "Error invalid character :'"
                    + tempString.at(j)
                    + "' in string!\n");
            }
            // >> is the bitwise right shift.
            bitIndicator = bitIndicator >> 1;
        }
        resultVector[i] = number;
        bitIndicator    = BIT_INDICATOR;
        number          = 0;
    }
    return resultVector;
}

baseSnipSequence baseSnipSequence::mutateAPosition(
    const baseSnipSequence& aSnipSequence,
    size_t thePosition) {
    size_t blockNumber               = thePosition / BIT_SIZE_OF_DATA_TYPE;
    size_t positionInTheBlock        = thePosition % BIT_SIZE_OF_DATA_TYPE;
    DATA_TYPE bit1OnTheRightPosition = BIT_INDICATOR >> positionInTheBlock;

    string err_prefix = string(__FILE__) + ':' + std::to_string(__LINE__);
    if (thePosition >= aSnipSequence.getSizeOfSequence()) {
        throw std::logic_error(
            err_prefix
            + ": Invalid position, must be between 0 and size of sequence-1.");
    }
    if (!(aSnipSequence.snipsData[blockNumber] & bit1OnTheRightPosition)) {
        throw std::logic_error(err_prefix
                               + ": Can't mutate an primitive position!");
    }

    // Creates a nes Snip sequence
    std::valarray<DATA_TYPE> newSnipData = aSnipSequence.snipsData;
    // Flips the right bit!
    newSnipData[blockNumber] ^= bit1OnTheRightPosition;

    return baseSnipSequence(newSnipData, aSnipSequence.sizeOfSequence);
}

string baseSnipSequence::toString(const baseSnipSequence& aSeq) {
    size_t residualSequenceSize
        = aSeq.getSizeOfSequence() % BIT_SIZE_OF_DATA_TYPE;
    string result          = "";
    DATA_TYPE bitIndicator = BIT_INDICATOR;
    DATA_TYPE aSnipsBlock  = 0;

    for (size_t i = 0; i < aSeq.snipsData.size(); ++i) {
        aSnipsBlock = aSeq.snipsData[i];
        for (size_t j = 0; j < BIT_SIZE_OF_DATA_TYPE; ++j) {
            aSnipsBlock& bitIndicator ? result += "1" : result += "0";
            bitIndicator = bitIndicator >> 1;
        }
        bitIndicator = BIT_INDICATOR;
    }
    if (residualSequenceSize == 0) { return result; }
    else {
        return result.substr(
            0, result.size() - (BIT_SIZE_OF_DATA_TYPE - residualSequenceSize));
    }
}
