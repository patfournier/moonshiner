;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((c-mode
  (mode . c++))
 (nil
  (flycheck-gcc-language-standard . "c++1z"))
 (nil
  (flycheck-clang-language-standard . "c++1z")))
