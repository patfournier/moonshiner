/*
 * baseSnipSequence.h
 *
 *  Created on: 2018-03-15
 *      Author: Éric Marcotte & Patrick Fournier
 *
 *  This class contains attributes and methods shared among every SnipSequence
 *  classes.
 */

#ifndef BASESNIPSEQUENCE_H_
#define BASESNIPSEQUENCE_H_

#include <cassert>
#include <random>
#include <stdexcept>
#include <string>
#include <valarray>
#include <vector>

// 32 bits !
/*
#define BIT_INDICATOR 0x80000000
#define ALLONES 0xffffffff
#define BIT_SIZE_OF_DATA_TYPE 32
#define DATA_TYPE uint32_t
*/

// 64 bits !
#define BIT_INDICATOR 0x8000000000000000
#define ALLONES 0xffffffffffffffff
#define BIT_SIZE_OF_DATA_TYPE 64
#define DATA_TYPE uint64_t

using std::ostream;
using std::string;
using std::valarray;
using std::vector;

class baseSnipSequence {
public:
    /***** Public attributes *****/
    valarray<DATA_TYPE> snipsData;

    /***** Public methods *****/
    // Setters and getters for the private static data members.
    size_t getSizeOfSequence() const { return this->sizeOfSequence; }
    size_t getnumberOfBlock() const { return this->numberOfBlock; }

    // Checks if a certain position if a mutant snip for a sequence.
    static bool isPositionMutant(size_t thePosition,
                                 const baseSnipSequence& aSnipSequence) {
        return aSnipSequence.snipsData[(thePosition / BIT_SIZE_OF_DATA_TYPE)]
               & (BIT_INDICATOR >> (thePosition % BIT_SIZE_OF_DATA_TYPE));
    }

    // Modifiers.
    static baseSnipSequence mutateAPosition(const baseSnipSequence&, size_t);

    // Converter
    static valarray<DATA_TYPE> convertStringToFastSequence(string aString);
    static string toString(const baseSnipSequence& aSeq);

    // Overloading of + operator.
    // Will coalesce two sequences, regardless of compatibility.
    // If there is an coalescence between a primitive and a mutant marker,
    // the result will be an mutant marker.
    friend baseSnipSequence operator+(const baseSnipSequence& S1,
                                      const baseSnipSequence& S2) {
        return baseSnipSequence(S2.snipsData | S1.snipsData,
                                S1.getSizeOfSequence());
    }

    // Overloading of << operator.
    friend ostream& operator<<(ostream& out, const baseSnipSequence& aSeq) {
        return out << baseSnipSequence::toString(aSeq);
    }

    // Overloading of == operator.
    virtual bool operator==(const baseSnipSequence& otherSnipSequence) const {
        const std::valarray<DATA_TYPE>& S1 = this->snipsData;
        const std::valarray<DATA_TYPE>& S2 = otherSnipSequence.snipsData;

        std::valarray<bool> eq{S1 == S2};
        for (auto el : eq)
            if (!el) return false;

        return true;
    };

    // Overloading of != operator.
    virtual bool operator!=(const baseSnipSequence& otherSnipSequence) const {
        return !operator==(otherSnipSequence);
    };

protected:
    // Constructors.
    baseSnipSequence() {
        this->numberOfBlock  = 0;
        this->sizeOfSequence = 0;
    };
    baseSnipSequence(string aSnipDataSequence);
    baseSnipSequence(valarray<DATA_TYPE> snipsDataSequence,
                     size_t sizeOfSequence);

    baseSnipSequence(const baseSnipSequence& other)
        : snipsData{other.snipsData}
        , sizeOfSequence{other.sizeOfSequence}
        , numberOfBlock{other.numberOfBlock} {};

    // Protected members set in constructors with public getters.
    size_t sizeOfSequence;
    size_t numberOfBlock;
};

#endif /*BASESNIPSEQUENCE_H_*/
