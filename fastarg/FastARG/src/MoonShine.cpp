/*
 * MoonShine.cpp
 *
 *  Created on: 2017-03-07
 *      Author: Patrick Fournier
 */

#include "MoonShine.h"

#include <numeric>

void MoonShine::buildFirstTree(mt19937& eng) {
    using helpers::emplace_at_random;
    using helpers::shuffle;
    using std::discrete_distribution;
    using std::move;

    // Populate live nodes lists.
    coalTree_ptrs leaves;
    leaves.reserve(numberOfSequences);
    for (CoalTree& leaf : _trees) leaves.push_back(&leaf);
    shuffle(leaves, eng);

    std::forward_list<CoalTree*> wildLiveNodes, derivedLiveNodes;
    size_t nWild = 0, nDerived = 0;

    for (CoalTree* leaf : leaves) {
        if (leaf->isPositionMutant(0)) {
            derivedLiveNodes.emplace_front(move(leaf));
            ++nDerived;
        }
        else {
            wildLiveNodes.emplace_front(move(leaf));
            ++nWild;
        }
    }

    // As long as no mutation is possible, generate random coalescence.
    while (nDerived > 1) {
        enum coalescence : bool { WILD = false, MUTANT = true };

        coalescence coalescenceType{MUTANT};
        if (nWild > 1) {
            coalescenceType
                = static_cast<coalescence>(discrete_distribution<bool>{
                    static_cast<double>(nWild),
                    static_cast<double>(nDerived)}(eng));
        }

        if (coalescenceType == WILD) {
            generateCoalescence(wildLiveNodes, nWild, nWild + nDerived, eng);
            --nWild;
        }
        else if (coalescenceType == MUTANT) {
            generateCoalescence(
                derivedLiveNodes, nDerived, nWild + nDerived, eng);
            --nDerived;
        }
    }

    // Updates wild live nodes vector;
    if (nDerived > 0) {
        emplace_at_random(wildLiveNodes, derivedLiveNodes.front(), nWild, eng);
        ++nWild;
    }

    // The only thing left to do is to generate coalescence events until the
    // MRCA is reached.
    CoalTree* lastConstructedNode;
    while (nWild > 1) {
        lastConstructedNode
            = generateCoalescence(wildLiveNodes, nWild, nWild, eng);
        --nWild;
    }

    store_mrca(0, lastConstructedNode);
    store_gmrca(lastConstructedNode);
    _marginalTrees_idx.emplace_back(0);

#ifndef NDEBUG
    // Makes sure that the current marginal tree is coherent.
    size_t count{0};
    mrca(0).tree_coherent(0, count);

    assert(count == 2 * numberOfSequences - 1);

    mrcas_coherent(numberOfSequences);
#endif
}

CoalTree* MoonShine::generateCoalescence(listNodes_ptr& nodes,
                                         size_t nodesLength,
                                         size_t nbLiveNodes,
                                         mt19937& eng) {
    using helpers::emplace_at_random;
    using std::log;
    using std::move;

    // Make the first two nodes coalesce.
    nodes_pair childNodes;
    for (auto& child : childNodes) {
        child = nodes.front();
        nodes.pop_front();
    }

    CoalTree* newNode{
        _trees.emplace_back(move(childNodes), nbLiveNodes, *this, eng)};

    // Insert the new node at a random position.
    emplace_at_random(nodes, newNode, nodesLength - 2, eng);

    /* Probabilities */
    // MoonShine.
    add_log_prob_moonShine(-log(static_cast<long double>(nodesLength)));

    return newNode;
}

vector<SequentialSnipSequence> MoonShine::populateHaplotypes(
    const vector<ARGnode>& leaves) {
    vector<SequentialSnipSequence> haplotypes;
    haplotypes.reserve(numberOfSequences);

    for (const auto& leaf : leaves)
        haplotypes.emplace_back(SequentialSnipSequence(
            leaf.sequence.snipsData, leaf.sequence.getSizeOfSequence()));

    return haplotypes;
}

void MoonShine::run(mt19937& eng) {
    using std::accumulate;
    using std::discrete_distribution;
    using std::find;
    using std::get;
    using std::log;
    using std::replace;
    using std::slice;

    // Builds the first tree.
    buildFirstTree(eng);

    // We check for any non polymorphic derived positions.
    std::vector<size_t> non_polymorphic;
    non_polymorphic.reserve(lenghtOfSequences);
    for (size_t pos{1}; pos < lenghtOfSequences; ++pos)
        if (mrca(0)[pos]) non_polymorphic.emplace_back(pos);

    // First position for which the last tree is consistent.
    size_t firstPosLastTree = 0;
    // Number of blocks in a sequence.
    size_t nbBlocks = lenghtOfSequences / BIT_SIZE_OF_DATA_TYPE;
    if (lenghtOfSequences % BIT_SIZE_OF_DATA_TYPE != 0) ++nbBlocks;

    for (size_t pos = 1; pos < lenghtOfSequences; ++pos) {
        /* Finds the next inconsistent position. */
        // Mask for the positions left (inclusive) of firstPosLastTree.
        size_t firstLiveBlock = (firstPosLastTree + 1) / BIT_SIZE_OF_DATA_TYPE;
        DATA_TYPE firstLiveBlockLeftMask
            = ALLONES >> ((firstPosLastTree + 1) % BIT_SIZE_OF_DATA_TYPE);

        valarray<DATA_TYPE> mmnMaskSnpData(ALLONES, nbBlocks);
        mmnMaskSnpData[slice(0, firstLiveBlock, 1)] = 0;
        mmnMaskSnpData[firstLiveBlock] &= firstLiveBlockLeftMask;

        for (auto pos_ : non_polymorphic) {
            size_t block{pos_ / BIT_SIZE_OF_DATA_TYPE};
            size_t entry{pos_ % BIT_SIZE_OF_DATA_TYPE};

            mmnMaskSnpData[block] &= ~(BIT_INDICATOR >> entry);
        }

        SequentialSnipSequence mmnMask(mmnMaskSnpData, lenghtOfSequences);

        // Compute mmnXors.
        std::vector<std::pair<CoalTree*, SequentialSnipSequence>> mmnXors;
        mmnXors.reserve(2 * numberOfSequences - 1);
        for (CoalTree& leaf : _trees)
            leaf.compute_mmnXor(mmnXors, pos, mmnMask);

        // Find the next incompatible position and the corresponding mutation
        // edges.
        std::vector<CoalTree*> derivedEdges;
        derivedEdges.reserve(numberOfSequences);

        for (; derivedEdges.size() < 2 && pos < lenghtOfSequences; ++pos) {
            derivedEdges.clear();

            for (auto& pair : mmnXors) {
                CoalTree& node{*pair.first};
                SequentialSnipSequence& sequence{pair.second};

                if (sequence[pos]) derivedEdges.emplace_back(&node);
            }
        }
        --pos;

        // Checks if we're done.
        if (derivedEdges.size() < 2) return;

        /* Recombination and recoalescence */
        while (derivedEdges.size() > 1) {
            // Compute the height of the two highest dads and finds the derived
            // edge with the highest dad.
            std::array<double, 2> mDR{0.0, 0.0};
            CoalTree* deWithHighestDad{nullptr};
            mrca(pos).highestDads(mDR, deWithHighestDad, pos);

            // Select a recombination edge.
            std::vector<double> de_weights;
            de_weights.reserve(derivedEdges.size());
            std::transform(derivedEdges.begin(),
                           derivedEdges.end(),
                           std::back_inserter(de_weights),
                           [pos, deWithHighestDad, mDR](CoalTree* x) -> double {
                               double w1{x->weight(pos, deWithHighestDad, mDR)};

                               double w2{x->brother(pos)->weight(
                                   pos, deWithHighestDad, mDR)};

                               return w1 + w2;
                           });

            // Generates recombination point.
            CoalTree* recEdge;
            double recHeight;
            CoalTree* recCandidate;
            size_t recCandidateIdx;

            recCandidateIdx = discrete_distribution<size_t>(
                de_weights.begin(), de_weights.end())(eng);
            recCandidate = derivedEdges[recCandidateIdx];

            std::tie(recEdge, recHeight) = recCandidate->generateRecombination(
                pos, deWithHighestDad, mDR, eng);

            /* Probabilities */
            // MoonShine.
            add_log_prob_moonShine(
                log(static_cast<long double>(
                    recEdge->weight(pos, deWithHighestDad, mDR)))
                - log(accumulate(de_weights.begin(), de_weights.end(), 0.0)));

            derivedEdges.erase(derivedEdges.begin() + recCandidateIdx);

            double trunc;
            if (recEdge->isPositionMutant(pos)) {
                trunc = recEdge == deWithHighestDad ? get<0>(mDR) : get<1>(mDR);
            }
            else {
                trunc = std::numeric_limits<double>::infinity();
            }

            _trees.emplace_back(pos,
                                *recEdge,
                                recHeight,
                                trunc,
                                firstPosLastTree,
                                SnipsCoordinates,
                                eng);

            if (firstPosLastTree != _marginalTrees_idx.back())
                _marginalTrees_idx.emplace_back(firstPosLastTree);

            _true_nbNodes += 2;

            /* Updates derivedEdges. */

            std::vector<CoalTree*> toRemove;
            CoalTree* newDerivedEdge_{
                recCandidate->dad(pos)->newDerivedEdge(toRemove, pos)};
            if (auto newDeIt{find(
                    derivedEdges.begin(), derivedEdges.end(), newDerivedEdge_)};
                newDeIt == derivedEdges.end()) {
                replace(derivedEdges.begin(),
                        derivedEdges.end(),
                        recCandidate->brother(pos),
                        newDerivedEdge_);
            }
            else {
                for (auto de{derivedEdges.begin()}; de < derivedEdges.end();
                     ++de) {
                    if (recCandidate->brother(pos) == *de) {
                        derivedEdges.erase(de);
                        break;
                    }
                }
            }

            for (CoalTree* node : toRemove) {
                for (auto de{derivedEdges.begin()}; de < derivedEdges.end();
                     ++de) {
                    if (node == *de) {
                        derivedEdges.erase(de);
                        break;
                    }
                }
            }

            /* Probabilities */
            // ARG.
            // TODO: Find why derivedEdges is sometime empty.
            if (derivedEdges.size() < 2) {
                add_log_prob_arg(
                    newDerivedEdge_->prob_mutation(pos, SnipsCoordinates, eng));
            }

#ifndef NDEBUG
            // Makes sure that the current marginal tree is coherent.
            for (size_t k = firstPosLastTree + 1; k <= pos; ++k) {
                size_t count{0};
                mrca(k).tree_coherent(k, count);

                assert(count == 2 * numberOfSequences - 1);
            }

            mrcas_coherent(numberOfSequences);
#endif
        }
        /* Cleanup and adjustments */

        for (auto& pair : mmnXors) pair.first->mmnReset();

        firstPosLastTree = pos;
        if (firstPosLastTree != _marginalTrees_idx.back())
            _marginalTrees_idx.emplace_back(firstPosLastTree);
    }
}

double MoonShine::prob_pheno(size_t pos, bool log) {
    using std::exp;
    using std::get;

    std::vector<std::array<double, 2>> probs;
    probs.reserve(2 * numberOfSequences - 1);

    mrca(pos).prob_pheno(probs, pos, _totalPheno, _penetrance);

    double totalBranchLength{0};
    for (const auto& el : probs) totalBranchLength += get<1>(el);

    double log_totalProb{0};
    for (const auto& el : probs) log_totalProb += get<0>(el);

    for (const auto& el : probs)
        log_totalProb += get<0>(el) + std::log(get<1>(el));

    log_totalProb -= (2 * numberOfSequences - 2) * std::log(totalBranchLength);

    return log ? log_totalProb : exp(log_totalProb);
}

std::vector<long double> MoonShine::probs_pheno(bool log) {
    using std::adjacent_difference;
    using std::back_inserter;
    using std::exp;
    using std::fill_n;
    using std::transform;

    std::vector<long double> probs;
    probs.reserve(_marginalTrees_idx.size());

    transform(_marginalTrees_idx.begin(),
              _marginalTrees_idx.end(),
              back_inserter(probs),
              [this, log](size_t pos) -> long double {
                  return prob_pheno(pos, log);
              });

    std::vector<size_t> delta;
    delta.reserve(_marginalTrees_idx.size());

    adjacent_difference(_marginalTrees_idx.begin(),
                        _marginalTrees_idx.end(),
                        back_inserter(delta));

    std::vector<long double> ret;
    ret.reserve(lenghtOfSequences);

    for (size_t idx{0}; idx < _marginalTrees_idx.size() - 1; ++idx) {
        for (size_t k{0}; k < delta[idx + 1]; ++k) ret.emplace_back(probs[idx]);
    }

    fill_n(back_inserter(ret), lenghtOfSequences - ret.size(), probs.back());

    return ret;
}

CoalTree::nodes_map MoonShine::copy_mrcas(const MoonShine& other) {
    nodes_map ret{other._mrcas};

    for (auto& mrca : ret) {
        size_t dist{static_cast<size_t>(mrca.second - other._trees.begin())};

        mrca.second = _trees.begin() + dist;
    }

    return ret;
}

/***** Debug *****/
void MoonShine::output_tree(size_t pos, size_t idLength, size_t mult) {
    CoalTree::output_tree_helper(mrca(pos), pos, idLength, mult, 0);
}

void MoonShine::mrcas_coherent(size_t nbLeaves) {
    for (const auto& el : _mrcas) {
        size_t pos{el.first};
        CoalTree& mrca{*el.second};

        std::vector<CoalTree*> leaves;
        mrca.getLeaves(leaves, pos);
        assert(leaves.size() == nbLeaves);
    }
}
