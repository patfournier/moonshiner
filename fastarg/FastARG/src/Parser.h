/*
 * Parser.h
 *
 *  Created on: 2016-06-17
 *      Author: �ric Marcotte
 */

#ifndef PARSER_H_
#define PARSER_H_

#include <fstream>
#include <string>
#include <vector>

using std::ifstream;
using std::string;
using std::vector;

class Parser {
public:
	// Static Functions.
	static ifstream openFileStream( string fileName);
	static string getSNPSequenceOnTheNextLine( ifstream& aFileStream);
	static unsigned int nextUIntFromStream( ifstream& aFileStream);
	static bool nextZeroOrOne( ifstream& aFileStream);
	static void convertFile(ifstream& aFileStream,const string destinationFile);
	static void convertFileType2(ifstream& aFileStream,const string destinationFile);
	static void cleanData(ifstream& aFileStream,const string destinationFile);
	static void getPhenotypesData(ifstream& aFileStream);

	// Constructor
	Parser(){};

	// Destructor
	virtual ~Parser(){};
};

#endif /* PARSER_H_ */
