/*
 * Children.h
 *
 *  Created on: 2017-06-14
 *      Author: Patrick Fournier
 */

#pragma once

#include <set>

#include "SequentialSnipSequence.h"
#include "Types.h"

class Haplotypes {
public:
    /***** Constructors *****/

    Haplotypes(size_t pos, const SequentialSnipSequence& sequence)
        : _sequences{std::make_pair(pos, sequence)} {};

    Haplotypes(size_t pos,
               const SequentialSnipSequence& sequence1,
               const SequentialSnipSequence& sequence2)
        : Haplotypes(pos, sequence1 & sequence2){};

    Haplotypes() = default;
    Haplotypes(const Haplotypes&) = default;

    /***** Modifying methods *****/
    void insert_or_assign(size_t pos, const SequentialSnipSequence&& sequence) {
        _sequences.insert_or_assign(pos, sequence);
    };

    Haplotypes& operator=(Haplotypes&& rhs) {
        if (&rhs != this) {
            _sequences.empty();
            _sequences.swap(rhs._sequences);
        }

        return *this;
    };

    Haplotypes& operator=(const Haplotypes& other){
        _sequences = other._sequences;

        return *this;
    }

    /***** Accessors *****/
    SequentialSnipSequence& operator[](size_t pos) {
        return _sequences.lower_bound(pos)->second;
    };

    const SequentialSnipSequence& operator[](size_t pos) const {
        return _sequences.lower_bound(pos)->second;
    };

protected:
    typedef std::map<size_t, SequentialSnipSequence, std::greater<size_t>>
        sequences_map;

    sequences_map _sequences;
};
