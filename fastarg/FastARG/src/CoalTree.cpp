/*
 * CoalTree.cpp
 *
 *  Created on: 2018-03-09
 *      Author: Patrick Fournier
 */

#include <cmath>

#include "CoalTree.h"
#include "Dists.h"
#include "MoonShine.h"

CoalTree::CoalTree(Children::children&& children,
                   size_t nbLiveNodes,
                   const SequentialARG& parentARG,
                   mt19937& eng)
    : arg{std::get<0>(children)->arg}
    , haplotypes{0,
                 std::get<0>(children)->haplotypes[0],
                 std::get<1>(children)->haplotypes[0]}
    , children{0, std::forward<nodes_pair>(children)}
    , height(computeHeight(children, nbLiveNodes, parentARG, eng))
    , descendantStatus{initialize_descendantStatus(0, children)} {
    // Sets this node as the parent of its children.
    for (CoalTree* child : children) {
        nodes_map& childParents{child->parentCoalTree};

        childParents.emplace_hint(childParents.begin(),
                                  std::make_pair(0, this));
    }
}

CoalTree::CoalTree(size_t pos,
                   CoalTree& child,
                   double recombinationHeight,
                   double trunc,
                   size_t& leftLimit,
                   const std::vector<size_t>& snpCoordinates,
                   mt19937& eng)
    : arg{child.arg} {
    assert(recombinationHeight >= child.height);

    using dists::exponential_distribution;
    using dists::exponential_survival;
    using dists::uniform_survival;
    using helpers::remove_if;
    using std::accumulate;
    using std::get;
    using std::log;
    using std::make_pair;
    using std::max;
    using std::numeric_limits;

    /***** Find a brother *****/
    // possibleRecoalescence0 is only used to compute the rate parameter
    // of the exponential distribution.
    std::vector<CoalTree*> possibleRecoalescences0;

    child.findPossibleRecoalescences(
        possibleRecoalescences0, pos, arg->mrca(pos), recombinationHeight);
    size_t lambda{possibleRecoalescences0.size()};
    assert(lambda > 0);

    // Generates recoalescence height and finds possible recoalescences.
    double recoalescenceHeight_{exponential_distribution<double>(
        lambda, trunc, recombinationHeight)(eng)};
    double recoalescenceHeight{recoalescenceHeight_};
    assert(recoalescenceHeight >= recombinationHeight);
    assert(recoalescenceHeight <= trunc);

    std::vector<CoalTree*> possibleRecoalescences;
    child.findPossibleRecoalescences(
        possibleRecoalescences, pos, arg->mrca(pos), recoalescenceHeight);
    assert(possibleRecoalescences.size() > 0);

    // Pick a brother.
    size_t otherIdx{std::uniform_int_distribution<size_t>(
        0, possibleRecoalescences.size() - 1)(eng)};
    CoalTree& other{*possibleRecoalescences[otherIdx]};

    // Chooses a position for the recombination.

    long double recPosWeight{1};
    long double recPosWeight_tot{1};
    if (leftLimit < pos - 1) {
        std::vector<size_t> candidatePositions{
            snpCoordinates.begin() + leftLimit,
            snpCoordinates.begin() + pos + 1};
        std::vector<double> recPointsWeights;
        recPointsWeights.reserve(pos - leftLimit + 1);
        std::adjacent_difference(candidatePositions.begin(),
                                 candidatePositions.end(),
                                 back_inserter(recPointsWeights));

        size_t recShift{std::discrete_distribution<size_t>(
            recPointsWeights.begin() + 1, recPointsWeights.end())(eng)};
        pos       = leftLimit + 1 + recShift;
        leftLimit = pos;

        recPosWeight     = recPointsWeights[recShift + 1];
        recPosWeight_tot = accumulate(
            recPointsWeights.begin() + 1, recPointsWeights.end(), 0);
    }

    /***** Instance attributes *****/
    if (!other.isMrca(pos))
        parentCoalTree.insert(make_pair(pos, other.dad(pos)));

    height = recoalescenceHeight;

    children = {pos, &child, &other};

    descendantStatus = initialize_descendantStatus(pos, {&child, &other});
    haplotypes.insert_or_assign(pos,
                                child.haplotypes[pos] & other.haplotypes[pos]);

    /***** Attributes of other instances *****/
    // Child.
    CoalTree& childOldDad{*child.dad(pos)};
    CoalTree* childOldGrandad{childOldDad.dad(pos)};
    CoalTree& childOldBrother{*child.brother(pos)};
    child.parentCoalTree.insert_or_assign(pos, this);

    // Child's grandad.
    if (childOldDad.isMrca(pos))
        arg->store_mrca(pos, &childOldBrother);
    else {
        childOldGrandad->children.insert_or_assign(
            pos, childOldGrandad->children[pos]);
        childOldGrandad->children.substitute(
            pos, &childOldDad, &childOldBrother);

        childOldBrother.parentCoalTree.insert_or_assign(pos, childOldGrandad);
    }

    // Other.
    CoalTree* otherOldDad{other.dad(pos)};
    other.parentCoalTree.insert_or_assign(pos, this);

    // Other's dad.
    if (other.isMrca(pos)) {
        arg->store_mrca(pos, this);
        if (other.isGmrca()) { arg->store_gmrca(this); }
    }
    else {
        otherOldDad->children.insert_or_assign(pos, otherOldDad->children[pos]);
        otherOldDad->children.substitute(pos, &other, this);
    }

    if (childOldGrandad) {
        childOldGrandad->update_haplotype(pos);
        childOldGrandad->update_descendantStatus(pos);
    }
    if (otherOldDad) {
        otherOldDad->update_haplotype(pos);
        otherOldDad->update_descendantStatus(pos);
    }

    /* Probabilities */
    // MoonShine.
    // Recombination.
    arg->add_log_prob_moonShine(log(recPosWeight) - log(recPosWeight_tot));

    // Recoalescence.
    long double surv{exponential_survival(
        recoalescenceHeight, lambda, trunc, recombinationHeight)};
    long double otherProb{
        static_cast<long double>(possibleRecoalescences.size())};

    arg->add_log_prob_moonShine(surv - log(otherProb));

    // ARG.
    // Recombination.
    long double k0{static_cast<double>(nb_liveNodes(arg, recombinationHeight))};
    long double arg_lambda0{(k0 / 2) * (k0 - 1 + arg->theta + arg->rho)};

    arg->add_log_prob_arg(exponential_survival(recombinationHeight, arg_lambda0)
                          + uniform_survival(snpCoordinates[pos],
                                             snpCoordinates.front(),
                                             snpCoordinates.back())
                          + log(arg->rho) - log(k0 - 1 + arg->theta + arg->rho)
                          - log(k0));

    // Recoalescence.
    long double k1{static_cast<double>(nb_liveNodes(arg, recoalescenceHeight))};
    long double arg_lambda1{(k1 / 2) * (k1 - 1 + arg->theta + arg->rho)};

    arg->add_log_prob_arg(
        log(2) + exponential_survival(recoalescenceHeight, arg_lambda1)
        - log(k1 - 1 + arg->theta + arg->rho));
};

void CoalTree::compute_mmnXor(
    std::vector<std::pair<CoalTree*, SequentialSnipSequence>>& mmnXors,
    size_t pos,
    const SequentialSnipSequence& leftMask) {
    using std::make_pair;

    // If the node has already been visited, nothing to do.
    if (mmnVisited) return;
    mmnVisited = true;

    // If the masked haplotype is 0, no mutation can happen upstream.
    if (const SequentialSnipSequence& haplotype_ = haplotypes[pos] & leftMask;
        haplotype_.all0())
        return;

    mmnXors.emplace_back(
        make_pair(this, haplotypes[pos] ^ dad(pos)->haplotypes[pos]));

    dad(pos)->compute_mmnXor(mmnXors, pos, leftMask);
};

void CoalTree::findPossibleRecoalescences(
    std::vector<CoalTree*>& possibleRecoalescences,
    const size_t pos,
    CoalTree& candidate,
    const double recHeight) const {
    assert(recHeight >= height);

    if (candidate.height <= recHeight) {
        if (candidate.isMrca(pos) && &candidate != dad(pos)
            && isPositionMutant(pos) == candidate[pos]) {
            possibleRecoalescences.emplace_back(&candidate);
        }

        return;
    }

    for (CoalTree* child : candidate.children[pos]) {
        if (child != this) {
            if (child->height <= recHeight && child != dad(pos)
                && isPositionMutant(pos) == child->isPositionMutant(pos)) {
                possibleRecoalescences.emplace_back(child);
            }
            else if (child->height >= recHeight) {
                findPossibleRecoalescences(
                    possibleRecoalescences, pos, *child, recHeight);
            }
        }
    }
}

void CoalTree::highestDads(std::array<double, 2>& mDR,
                           CoalTree*& deWithHighestDad,
                           size_t pos) {
    using std::get;

    if (!isMrca(pos) && isPositionMutant(pos)) {
        double dadHeight{dad(pos)->height};

        if (dadHeight > get<0>(mDR)) {
            if (dadHeight > get<1>(mDR)) {
                double tmp{get<1>(mDR)};
                get<1>(mDR)      = dadHeight;
                get<0>(mDR)      = tmp;
                deWithHighestDad = this;
            }
            else {
                get<0>(mDR) = dadHeight;
            }
        }
        return;
    }

    if (isLeaf()) return;

    for (auto& child : children[pos])
        child->highestDads(mDR, deWithHighestDad, pos);
}

void CoalTree::output_tree_helper(CoalTree& node,
                                  size_t pos,
                                  size_t idLength,
                                  size_t mult,
                                  size_t offset) {
    using std::cout, std::stringstream;
    using std::endl;
    using std::get;
    using std::string;

    const bool leaf{node.isLeaf()};

    offset += mult * idLength;

    const string space{string(offset, ' ')};

    stringstream ss;
    ss << static_cast<const void*>(&node);
    const string label{ss.str().substr(ss.str().size() - idLength, idLength)};

    if (!leaf)
        output_tree_helper(
            *get<0>(node.children[pos]), pos, idLength, mult, offset);

    cout << endl << space;
    leaf ? cout << "/" : cout << " ";
    node[pos] ? cout << "*" : cout << " ";
    cout << label;
    node[pos] ? cout << "*" : cout << " ";
    leaf ? cout << "/" : cout << "  ";

    cout << endl;

    if (!leaf)
        output_tree_helper(
            *get<1>(node.children[pos]), pos, idLength, mult, offset);
}

void CoalTree::tree_coherent(size_t pos, size_t& count) {
    using std::back_inserter;
    using std::for_each;
    using std::get;
    using std::transform;

    // Counts the number of nodes visited.
    ++count;

    if (!isMrca(pos)) {
        // Makes sure that the "brother of" relation is symmetrical.
        assert(brother(pos)->brother(pos) == this);

        // Makes sure that every node is bellow its father.
        assert(height <= dad(pos)->height);
    }

    // Makes sure that descendantStatus is coherent.
    std::vector<CoalTree*> leaves;
    getLeaves(leaves, pos);
    size_t nControls{static_cast<size_t>(std::count_if(
        leaves.begin(), leaves.end(), [pos](CoalTree* leaf) -> bool {
            return get<0>(leaf->descendantStatus.lower_bound(pos)->second);
        }))};
    size_t nCases{static_cast<size_t>(std::count_if(
        leaves.begin(), leaves.end(), [pos](CoalTree* leaf) -> bool {
            return get<1>(leaf->descendantStatus.lower_bound(pos)->second);
        }))};

    std::array<size_t, 2> test_ds{nControls, nCases};
    assert(descendantStatus.lower_bound(pos)->second == test_ds);

    // Makes sure that haplotype is coherent.
    std::vector<SequentialSnipSequence> sequences;
    transform(leaves.begin(),
              leaves.end(),
              back_inserter(sequences),
              [pos](CoalTree* leaf) -> SequentialSnipSequence {
                  return leaf->haplotypes[pos];
              });

    SequentialSnipSequence test_hap{sequences.back()};
    sequences.pop_back();
    // clang-format off
        for_each(sequences.begin(),
                 sequences.end(),
                 [&test_hap](const SequentialSnipSequence& sequence) -> void {
                     test_hap &= sequence;
                 });
    // clang-format on
    assert(haplotypes[pos] == test_hap);

    if (!isLeaf()) {
        get<0>(children[pos])->tree_coherent(pos, count);
        get<1>(children[pos])->tree_coherent(pos, count);
    }
}

double CoalTree::maxRecoalescenceHeight(size_t pos) {
    using std::get;
    using std::max;

    if (isLeaf()) return 0;
    if (isPositionMutant(pos)) return height;

    std::array<double, 2> childrenMaxHeight;
    for (size_t k{0}; k < 2; ++k)
        childrenMaxHeight[k] = children[pos][k]->maxRecoalescenceHeight(pos);

    return max(get<0>(childrenMaxHeight), get<1>(childrenMaxHeight));
}

void CoalTree::leftLimit_helper(size_t pos,
                                size_t leftmost,
                                size_t& leftLimit) {
    assert(leftmost <= pos);

    using std::max;

    leftLimit = max(lastTree(), leftmost);
    if (leftLimit == pos) return;

    if (dad(pos) && !dad(pos)->isMrca(pos))
        dad(pos)->leftLimit_helper(pos, leftmost, leftLimit);
}

std::pair<CoalTree*, double> CoalTree::generateRecombination(
    size_t pos,
    CoalTree* deWithHighestDad,
    const std::array<double, 2>& mDR,
    mt19937& eng) {
    using dists::uniform_survival;
    using std::get;
    using std::uniform_real_distribution;

    assert(get<0>(mDR) <= get<1>(mDR));

    double thisWeight{weight(pos, deWithHighestDad, mDR)};
    double b{thisWeight + brother(pos)->weight(pos, deWithHighestDad, mDR)};

    if (b == 0.0) return {};

    double uniBranches = uniform_real_distribution<double>(0, b)(eng);

    CoalTree* recNode;
    double recHeight;
    if (uniBranches <= thisWeight) {
        recNode   = this;
        recHeight = height + uniBranches;
    }
    else {
        recNode   = brother(pos);
        recHeight = brother(pos)->height + uniBranches - thisWeight;
    }

    /* Probabilities */
    arg->add_log_prob_moonShine(uniform_survival(uniBranches, 0.0, b));

    return {recNode, recHeight};
};

bool CoalTree::isMrca(size_t pos) const {
    return this == &arg->mrca(pos);
};

void CoalTree::prob_pheno_helper(std::vector<std::array<double, 2>>& probs,
                                 size_t pos,
                                 const std::array<size_t, 2>& totalPheno,
                                 const std::array<double, 2>& penetrance) {
    using std::get;
    using std::log;
    using std::make_pair;

    double f0{get<0>(penetrance)};
    double f1{get<1>(penetrance)};

    size_t controls_carier{get<0>(descendantStatus.lower_bound(pos)->second)};
    size_t cases_carier{get<1>(descendantStatus.lower_bound(pos)->second)};
    size_t controls_nonCarier{get<0>(totalPheno) - controls_carier};
    size_t cases_nonCarier{get<1>(totalPheno) - cases_carier};

    double log_prob{cases_carier * log(f1) + controls_carier * log(1 - f1)
                    + cases_nonCarier * log(f0)
                    + controls_nonCarier * log(1 - f0)};

    double weight{dad(pos)->height - height};

    probs.emplace_back(std::array<double, 2>{log_prob, weight});

    if (isLeaf()) return;

    for (CoalTree* child : children[pos])
        child->prob_pheno_helper(probs, pos, totalPheno, penetrance);
}

CoalTree* CoalTree::newDerivedEdge(std::vector<CoalTree*>& toRemove,
                                   size_t pos) {
    if (brother(pos)->isPositionMutant(pos)) {
        toRemove.emplace_back(brother(pos));
        return dad(pos)->newDerivedEdge(toRemove, pos);
    }

    return this;
}

CoalTree::nodes_map CoalTree::copy_parentCoalTree(const CoalTree& other) {
    size_t ptr_offset{static_cast<size_t>(this - &other)};

    nodes_map ret{other.parentCoalTree};
    for (auto& parent : ret) parent.second += ptr_offset;

    return ret;
}

Children CoalTree::copy_children(const CoalTree& other) {
    using std::get;

    size_t ptr_offset{static_cast<size_t>(this - &other)};

    Children ret{other.children};

    for (auto& children : ret)
        for (auto& child : children.second) child += ptr_offset;

    return ret;
}

double CoalTree::computeHeight(const std::array<CoalTree*, 2>& children,
                               size_t nbLiveNodes,
                               const SequentialARG& parentARG,
                               mt19937& eng) {
    using dists::exponential_survival;
    using std::get;
    using std::log;

    double k      = nbLiveNodes;
    double theta  = parentARG.theta;
    double rho    = parentARG.rho;
    double lambda = (k / 2) * (k - 1 + theta + rho);

    assert(k > 1);

    std::exponential_distribution<double> waitingTime(lambda);
    double timeToOldestNode = waitingTime(eng);

    double height{
        timeToOldestNode
        + std::max(get<0>(children)->height, get<1>(children)->height)};

    /* Probabilities */
    // ARG.
    arg->add_log_prob_arg(log(static_cast<long double>(2))
                          - log(static_cast<long double>(k - 1 + theta + rho)));

    return height;
};

double CoalTree::weight(size_t pos,
                        CoalTree* limitEdge,
                        const std::array<double, 2>& mDR) const {
    using std::get;
    using std::max;
    using std::min;

    if (isPositionMutant(pos)) {
        if (this == limitEdge)
            return max(get<0>(mDR) - height, 0.0);
        else
            return min(dad(pos)->height, get<1>(mDR)) - height;
    }
    // Since the brother of the recoalescence node becomes the child of
    // its grandad after the recoalescence, the brother of *this is
    // allowed to recombine if its uncle is derived. Note that if *this
    // is the child of the mrca, no recombination can happen on its
    // brother since no recoalescence would be possible; recombination
    // on the brother is not allowed in that case. Also, if *this is the
    // grandchild of the mrca, recombination is prohibited on its
    // brother since the 2 children of the mrca would have derived
    // allele.
    else if (CoalTree * uncle_{brother(pos)->uncle(pos)}; uncle_) {
        if (uncle_->isPositionMutant(pos) && !dad(pos)->isMrca(pos)
            && !grandad(pos)->isMrca(pos)) {
            // Recoalescence must be possible at brother's height.
            std::vector<CoalTree*> possibleCoalescences;
            findPossibleRecoalescences(
                possibleCoalescences, pos, arg->mrca(pos), height);
            if (!possibleCoalescences.empty()) return dad(pos)->height - height;
        }
    }

    return 0.0;
};

size_t CoalTree::nb_liveNodes(MoonShine* arg, double height) {
    using std::max;
    using std::round;
    using std::sqrt;

    double prop_lower{height / arg->gmrca().height};
    assert(prop_lower >= 0);
    assert(prop_lower <= 1);

    double r{1.0 - sqrt(prop_lower)};
    double nb{arg->nbLeaves() * r};

    return max(static_cast<size_t>(round(nb)), static_cast<size_t>(1));
}

long double CoalTree::prob_mutation(size_t pos,
                                    const std::vector<size_t>& snpsCoordinates,
                                    mt19937& eng,
                                    bool log) {
    using dists::exponential_survival;
    using dists::uniform_survival;
    using std::uniform_real_distribution;

    long double mutationHeight{
        uniform_real_distribution<long double>{height, dad(pos)->height}(eng)};

    long double k{static_cast<double>(nb_liveNodes(arg, mutationHeight))};
    long double arg_lambda{(k / 2) * (k - 1 + arg->theta + arg->rho)};

    long double log_prob{exponential_survival(mutationHeight, arg_lambda)
                         + uniform_survival(snpsCoordinates[pos],
                                            snpsCoordinates.front(),
                                            snpsCoordinates.back())
                         + std::log(arg->theta)
                         - std::log(k - 1 + arg->theta + arg->rho)};

    return log ? log_prob : exp(log_prob);
}

bool CoalTree::isGmrca() const {
    return this == &arg->gmrca();
}
