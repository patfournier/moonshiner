/*
 * MoonShine.h
 *
 *  Created on: 2017-03-07
 *      Author: Patrick Fournier
 */

#ifndef MOONSHINE_H_
#define MOONSHINE_H_

//#include <boost/progress.hpp>
//#include <set>
#include <Rcpp.h>
#include <algorithm>
#include <cmath>
#include <forward_list>
#include <iterator>
#include <numeric>
#include <random>
#include <tuple>

#include "ARGnode.h"
#include "CoalTree.h"
#include "CoalTreePool.h"
#include "Enums.h"
#include "Helpers.h"
#include "Parameters.h"
#include "SequentialARG.h"
#include "SequentialSnipSequence.h"
#include "Types.h"

class MoonShine : public SequentialARG, protected Types<CoalTree> {
public:
    /***** Public methods *****/
    /* Constructors */

    // Constructs a MoonShine instance from a vector of SequentialSnipSequences.
    MoonShine(const Parameters& params,
              const std::vector<SequentialSnipSequence>& leaves)
        : SequentialARG{params}
        , _trees{leaves,
                 phenotypes,
                 lenghtOfSequences * (2 * numberOfSequences - 1)
                     + numberOfSequences,
                 this}
        , _haplotypes{leaves}
        , _penetrance{params.penetrance[0], params.penetrance[1]}
        , _totalPheno{initialize_totalPheno(params.phenotypes)}
        , _true_nbNodes{0} {};

    MoonShine(const Parameters& params,
              std::vector<SequentialSnipSequence>&& leaves)
        : SequentialARG{params}
        , _trees{leaves,
                 phenotypes,
                 lenghtOfSequences * (2 * numberOfSequences - 1)
                     + numberOfSequences,
                 this}
        , _haplotypes{std::move(leaves)}
        , _penetrance{params.penetrance[0], params.penetrance[1]}
        , _totalPheno{initialize_totalPheno(params.phenotypes)}
        , _true_nbNodes{0} {};

    // Construct a MoonShine instance from ARGnodes. For compatibility with
    // FastARG.
    MoonShine(const Parameters& params, const vector<ARGnode>& leaves)
        : MoonShine{params, populateHaplotypes(leaves)} {};

    // This constructor is provided to be consistent with the ARG class. The
    // last two arguments are not used.
    MoonShine(const Parameters& params,
              const vector<ARGnode>& leaves,
              const vector<bool>& phenotypes,
              const vector<size_t>& coordinates)
        : MoonShine{params, populateHaplotypes(leaves)} {};

    // Move constructor
    MoonShine(MoonShine&& other)
        : SequentialARG{other.copy_parameters()}
        , _trees{std::move(other._trees)}
        , _mrcas{std::move(other._mrcas)}
        , _haplotypes{std::move(other._haplotypes)}
        , _marginalTrees_idx{std::move(other._marginalTrees_idx)}
        , _penetrance{std::move(other._penetrance)}
        , _totalPheno{std::move(other._totalPheno)}
        , _true_nbNodes{other._true_nbNodes} {
        // Update nodes arg pointer.
        _trees.update_arg(this);
    };

    // Copy constructor.
    MoonShine(const MoonShine& other)
        : SequentialARG{other.copy_parameters()}
        , _trees{other._trees}
        , _mrcas{copy_mrcas(other)}
        , _haplotypes{other._haplotypes}
        , _marginalTrees_idx{other._marginalTrees_idx}
        , _penetrance{other._penetrance}
        , _totalPheno{other._totalPheno}
        , _true_nbNodes{other._true_nbNodes} {
        // Update nodes arg pointer.
        _trees.update_arg(this);
    };

    // The method run build the ARG.
    void run() override { std::cerr << "Not implemented" << std::endl; };

    void run(mt19937& eng);

    /* Service methods needed by FastARG. */
    void printARGSummary() override {
        std::cout << "Nothing here!" << std::endl;
    };
    vector<uint16_t> getLeafReachabilityforALocus(
        size_t theLocusIndex,
        size_t nodeIndex) const override {
        std::cout << "Nothing here!" << std::endl;
        return {};
    };
    vector<long double> chi2AssociationTestResults() override {
        std::cout << "Nothing here!" << std::endl;
        return {};
    };

    // Returns the number of nodes in the ARG.
    size_t nbNodes() const { return _true_nbNodes; };

    // Returns the number of leaves in the ARG.
    size_t nbLeaves() const { return _haplotypes.size(); };

    // Returns a vector containing pointers to the leaves of the ARG.
    std::vector<CoalTree*> leaves() const {
        return {_trees.begin(), _trees.end()};
    }

    /* MRCA */
    // Gets the root of the tree to which the node belongs.
    CoalTree& mrca(size_t pos) { return *_mrcas.lower_bound(pos)->second; };

    // Gets the grand mrca.
    const CoalTree& gmrca() const { return *_gmrca; };

    // Store a node as mrca.
    void store_mrca(size_t pos, CoalTree* node) {
        _mrcas.insert_or_assign(pos, node);
    };

    // Store a node as grand mrca.
    void store_gmrca(CoalTree* node) {
#ifndef NDEBUG
        bool is_mrca{false};
        for (const auto& mrca : _mrcas)
            if (node == mrca.second) {
                is_mrca = true;
                break;
            }
        assert(is_mrca);
#endif

        _gmrca = node;
    }

    /* Probabilities */
    // Computes the probability of the phenotype vector for the marginal tree
    // associated with position pos.
    double prob_pheno(size_t pos, bool log = true);

    std::vector<long double> probs_pheno(bool log = true);

    void add_log_prob_moonShine(long double log_prob) {
        log_prob_moonShine += log_prob;
    };

    void add_log_prob_arg(long double log_prob) { log_prob_arg += log_prob; };

    // Returns the importance sampling weight of the graph.
    long double weight(bool log = true) {
        using std::exp;

        long double log_weight{log_prob_arg - log_prob_moonShine};

        return log ? log_weight : exp(log_weight);
    };

    /***** Debug *****/
    void output_tree(size_t pos, size_t idLength = 4, size_t mult = 2)
        __attribute__((noinline));

    void mrcas_coherent(size_t nbLeaves);

protected:
    /***** Protected attributes *****/
    // Reserved space to store trees.
    CoalTreePool _trees;

    // The second element of the pair is the leftmost position for which the
    // first element is the mrca. The pairs are stored in decreasing order of
    // position. Thus, the mrca of a position will be the first element for
    // which the position is "not less" (in the present case, that actually
    // means smaller or equal) than a given integer. That allows us to find the
    // mrca in logarithmic time using the lower_bound method.
    nodes_map _mrcas;

    CoalTree* _gmrca;

    vector<SequentialSnipSequence> _haplotypes;

    // Marginal trees.
    std::vector<size_t> _marginalTrees_idx;

    // Penetrance vector.
    std::array<double, 2> _penetrance;

    // Total number of sequence with each phenotype (0 = control).
    std::array<size_t, 2> _totalPheno;

    // Number of nodes in the ARG (including recombination nodes).
    size_t _true_nbNodes;

    /* Probabilities */
    long double log_prob_moonShine{0};

    long double log_prob_arg{0};

    /***** Protected methods *****/
    /* Helper methods for constructors */

    // Populates haplotypes vector.
    vector<SequentialSnipSequence> populateHaplotypes(
        const vector<ARGnode>& leaves);

    Parameters copy_parameters() const {
        Parameters parameters;
        parameters.sequencesSize     = lenghtOfSequences;
        parameters.numberOfSequences = numberOfSequences;
        parameters.phenotypes        = phenotypes;
        parameters.locusCoordinates  = SnipsCoordinates;

        parameters.recombinationModeChoice = recombinationModeFlag;

        return parameters;
    }

    std::array<size_t, 2> initialize_totalPheno(
        const std::vector<bool>& phenotypes) {
        using std::count;

        size_t nbCases{static_cast<size_t>(
            count(phenotypes.begin(), phenotypes.end(), true))};
        size_t nbControl{phenotypes.size() - nbCases};

        return {2 * nbControl, 2 * nbCases};
    };

    nodes_map copy_mrcas(const MoonShine& other);

    /* Methods used for tree building */

    void buildFirstTree(mt19937& eng);

    // Generate a coalescence from the front of a forward_list of possible
    // coalescence and returns a pointer to the newly created node.
    CoalTree* generateCoalescence(listNodes_ptr& nodes,
                                  size_t nodesLength,
                                  size_t nbLiveNodes,
                                  mt19937& eng);

    // Find mutant edges in the tree number "tree" to make it compatible with
    // position "position". "The second member of the pair is true if the first
    // branch of the first member is the mutant edge, false if it is the second
    // branch.
    std::vector<std::pair<CoalTree*, bool>> findMutantEdges(size_t tree,
                                                            size_t position);
};

#endif /*MOONSHINE_H_*/
