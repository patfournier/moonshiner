/*
 * SequentialARG.h
 *
 *  Created on: 2018-03-20
 *      Author: Patrick Fournier
 *
 *  Base class for sequentially built ARG.
 */

#ifndef SEQUENTIALARG_H_
#define SEQUENTIALARG_H_

#include "BaseARG.h"
#include "PopParameters.h"

class SequentialARG : public BaseARG, public PopParameters {
public:
    /***** Public methods *****/
    // Constructor.
    SequentialARG(const Parameters& params)
        : BaseARG(params)
        , PopParameters(params){};

    // Destructor.
    virtual ~SequentialARG(){};
};

#endif /*SEQUENTIALARG_H_*/
