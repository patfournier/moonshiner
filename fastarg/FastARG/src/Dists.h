/*
 * Dists.h
 *
 *  Created on: 2018-06-22
 *      Author: Patrick Fournier
 *
 *  Stuff related to probability distributions.
 */

#pragma once

#include <cmath>
#include <random>

namespace dists
{
/***** Truncated shifted exponential *****/

// Random generation.
template <class RealType = double>
class exponential_distribution {
public:
    typedef RealType result_type;

    exponential_distribution(RealType lambda, RealType trunc, RealType shift)
        : lambda{lambda}
        , trunc{trunc}
        , shift{shift} {};

    exponential_distribution(RealType lambda)
        : exponential_distribution(lambda,
                                   std::numeric_limits<RealType>::infinity(),
                                   0.0){};

    template <class Generator>
    result_type operator()(Generator& g) {
        assert(shift <= trunc);

        if (trunc > std::numeric_limits<RealType>::max())
            return shift + std::exponential_distribution<RealType>(lambda)(g);

        RealType u{std::uniform_real_distribution<RealType>()(g)};
        RealType invD{shift - trunc};
        return shift - log(1 - (1 - exp(lambda * invD)) * u) / lambda;
    }

private:
    double lambda;
    double trunc;
    double shift;
};

// Survival function.
long double exponential_survival(double x, double lambda, bool log = true) {
    if (log)
        return std::log(lambda) - lambda * x;
    else
        return lambda * std::exp(-lambda * x);
};

long double exponential_survival(double x,
                                 double lambda,
                                 double trunc,
                                 double shift,
                                 bool log = true) {
    using std::exp;

    if (log)
        // clang-format off
        return lambda * shift
               + std::log(exp(-lambda * x) - exp(-lambda * trunc))
               - std::log(1 - exp(-lambda * (trunc - shift)));
        //clang-format on
    else
        // clang-format off
        return (exp(-lambda * (x - shift))
                    - exp(-lambda * (trunc - shift)))
               / (1 - exp(-lambda * (trunc - shift)));
    // clang-format on
};

/***** Uniform distribution *****/

// Survival function.
long double uniform_survival(double x, double a, double b, bool log = true) {
    if (log)
        return std::log(x - a) - std::log(b - a);
    else
        return (x - a) / (b - a);
};

}  // namespace dists
