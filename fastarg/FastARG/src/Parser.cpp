/*
 * Parser.cpp
 *
 *  Created on: 2016-06-17
 *      Author: �ric Marcotte
 */

#include "Parser.h"
#include "SnipSequence.h"

#include <iostream>
#include <sstream>

using std::endl;
using std::stringstream;
using std::cerr;
using std::cout;
using std::invalid_argument;

ifstream Parser::openFileStream(string sourceFileName){
	ifstream stream;
	stream.open(sourceFileName.c_str());

	if (!stream.is_open() ){

		throw invalid_argument( string(__FILE__)+':'+ std::to_string(__LINE__)+ ": Parsing error : The file \"" + sourceFileName + "\" cannot be found!\n" );
	}
	return stream;
}

string Parser::getSNPSequenceOnTheNextLine(ifstream& aFileStream){
	char aChar = aFileStream.get();

	string result = "";
	while (aChar != '\n' && !aFileStream.eof()){
		if (aChar == '0' || aChar == '1'){
			result += aChar;
		}
		aChar = aFileStream.get();
	}
	return result;
}


unsigned int Parser::nextUIntFromStream(std::ifstream& aFileStream){

	unsigned int value=0;
	string aStringOfTheValue;
	char aChar = aFileStream.get();

	// Move the stream to the next non white-space caracter.
	while ( isspace(aChar) ){
		aChar = aFileStream.get();
	}
	if (aFileStream.eof()){
		throw std::out_of_range("End-Of-file");
	}

	do{
		if ( ! (aChar >= '0' && aChar <= '9') ){
			throw std::string("Parsing error : invalid character  '" + std::to_string(aChar) + "' while parsing. Expecting a positive number.");
		}
		value += (aChar-48);
		value *= 10;
		aChar = aFileStream.get();
	}while ( aChar >= '0' && aChar <= '9');
	return value/10;
}


bool Parser::nextZeroOrOne(ifstream& aFileStream){
	unsigned int nextUInt = nextUIntFromStream(aFileStream);

	if (nextUInt == 1){
		return true;
	} else if (nextUInt == 0){
		return false;
	} else {
		throw std::runtime_error("Parsing error : expecting a '0' or a '1' !" );
	}
}

void Parser::getPhenotypesData(ifstream& aFileStream){
	char aChar= ' ';
	cout << "Reading file..." << endl;

	vector<unsigned int> phenotypes;
	while (!aFileStream.eof()){
		try{
			Parser::nextUIntFromStream(aFileStream); //  int Id =  Discarded
			Parser::nextUIntFromStream(aFileStream); // bool TIMGenotype = Discarded
			phenotypes.push_back(Parser::nextUIntFromStream(aFileStream));
		}catch(std::out_of_range & Error){
			break;
		}
		aChar = aFileStream.get();
		while (aChar != '\n' && !aFileStream.eof()){
			aChar = aFileStream.get();
		}
	}

	if (phenotypes.size() %2 != 0){

		throw invalid_argument( string(__FILE__) +':'+ std::to_string(__LINE__)+ ": The phenotypes vector must be an even number.");
	}
	cout << "Phenotypes=[";
	for (size_t i = 0 ; i < (phenotypes.size()/2)-1 ;++i){
		cout << phenotypes.at(2*i) << ',';
	}
	cout << phenotypes.back() << ']' << endl;;

}

void Parser::convertFile(ifstream& aFileStream,const string destinationFile){

	string result = "";
	char aChar;
	cout << "Reading file..." << endl;
	while (!aFileStream.eof()){
		try{
			Parser::nextUIntFromStream(aFileStream); //  int Id =  Discarded
			Parser::nextUIntFromStream(aFileStream); // bool TIMGenotype = Discarded
			Parser::nextUIntFromStream(aFileStream); // bool TIMGenotype = Discarded
		}catch(std::out_of_range & Error){
			break;
		}
		aChar = aFileStream.get();
		while (aChar != '\n' && !aFileStream.eof()){
			if ( aChar == '0'){ result += "0 ";	}
			if ( aChar == '1'){	result += "1 ";	}
			aChar = aFileStream.get();
			if (aChar == '\n' || aFileStream.eof() ){
				result +="\n";
			}
		}
	}

	cout << "Generating :" << destinationFile << endl;
	std::ofstream outStream;
	outStream.open(destinationFile.c_str());
	if (outStream){
		outStream << result <<  endl;
	}else{
		throw std::runtime_error( string(__FILE__) +':'+ std::to_string(__LINE__) + "Error : opening stream of data aborted.");
	}
	outStream.close();

}

void Parser::convertFileType2(ifstream& aFileStream,const string destinationFile){

	//string result = "";
	char aChar;
	cout << "Reading file..." << endl;
	vector<vector<char>> data;
	while (!aFileStream.eof()){
		try{
			aChar = aFileStream.get();
			while ( aChar != 'A' && aChar != 'C' && aChar != 'G' && aChar != 'T' &&  !aFileStream.eof() ){
				//cout << "1:"<< aChar << " ";
				aChar = aFileStream.get();
			}

		}catch(std::out_of_range & Error){
			break;
		}
		aChar = aFileStream.get();
	//cout << "2:"<< aChar << " ";
		vector<char> aLocus = {};
		while ( aChar != '\n' &&  !aFileStream.eof() ){

			//cout << "2:"<< aChar << " ";
			if ( aChar == '0'){ aLocus.push_back('0');	}
			if ( aChar == '1'){	aLocus.push_back('1');	}
			aChar = aFileStream.get();
			if (aChar == '\n' || aFileStream.eof() ){
				data.push_back(aLocus);
				//cout << data.size() << " : Pushed back a vector of size " << aLocus.size() << endl;
				aLocus.clear();

			}
		}
		//cout << "Press key!" << endl;
		//std::cin.get();
	}

	size_t numberOfSequences = data.at(0).size();
	for ( size_t i = 0 ; i <  data.size(); ++i){
		if (numberOfSequences != data.at(i).size() ){
			throw ( string(__FILE__) +':'+ std::to_string(__LINE__)+  "Invalid number of sequences !");
		}
	}

	cout << "Detected " << numberOfSequences << " sequences of " << data.size() << " snps marquers!" << endl;

	cout << "Generating :" << destinationFile << endl;
	std::ofstream outStream;
	outStream.open(destinationFile.c_str());

	if (outStream){
		for ( size_t i = 0 ; i < numberOfSequences;  ++i){
			for ( size_t j = 0 ; j < data.size();  ++j){
				outStream << data.at(j).at(i) << ' ';
			}
			outStream  <<  endl;
		}
	}else{
		throw std::runtime_error("Error : opening stream of data aborted.");
	}
	outStream.close();

}


void Parser::cleanData(ifstream& aFileStream,const string destinationFile){
	vector<SnipSequence> data;
	SnipSequence aSnipSeq;
	string aStringSeq = "";

	// Gets the data in the file.
	while(!aFileStream.eof()){
		aStringSeq = getSNPSequenceOnTheNextLine(aFileStream);
		if (aStringSeq.size() !=0){
			aSnipSeq = SnipSequence(aStringSeq);
			data.push_back(aSnipSeq);
		}
	}

	cout << "Detected " << data.size() << " sequences." << endl;

	//vector<size_t> numberOfSnipsForEachLoci = SnipSequence::computeNumberOfMutantSnipsForEachPosition(data);

	vector<size_t> numberOfSnipsForEachLoci(aSnipSeq.getSizeOfSequence());

	//this->numberOfMutantSNPsByPosition.resize(params.sequencesSize);

		for (const auto &aSeq : data){
			for (size_t i = 0 ; i < aSnipSeq.getSizeOfSequence(); ++i){
				if (SnipSequence::isPositionMutant(i, aSeq)){
					++numberOfSnipsForEachLoci.at(i);
				}
			}
		}

	string result="";

	for (size_t i = 0 ; i < data.size(); ++i){
		for (size_t j = 0 ; j < numberOfSnipsForEachLoci.size() ;++j){
			if(numberOfSnipsForEachLoci.at(j) != 0){
				if(SnipSequence::isPositionMutant(j,data.at(i)) ){
					result += "1 ";
				}else{
					result += "0 ";
				}
			}
		}
		result += "\n";
	}

	size_t numberOfPositionFiltered = 0;
	vector<unsigned int> filteredPosition;
	for (size_t i = 0 ; i < numberOfSnipsForEachLoci.size();++i){

		if(numberOfSnipsForEachLoci.at(i) == 0){
			filteredPosition.push_back(0);
			++numberOfPositionFiltered;
		}else{
			filteredPosition.push_back(1);
		}
	}

	cout << numberOfPositionFiltered << " position filtered." << endl;
	cout << "Position filtered : [";
	for (size_t i= 0 ; i < filteredPosition.size()-1;++i){
		cout << filteredPosition.at(i) << ",";
		if (i % 50 == 0){
			cout << endl;
		}
	}
	cout << filteredPosition.back() << "]" << endl;

	cout << "Generating :" << destinationFile << endl;
	std::ofstream outStream;
	outStream.open(destinationFile.c_str());
	if (outStream){
		outStream << result <<  endl;
	}else{
		throw std::runtime_error("Error : opening stream of data aborted.");
	}
	outStream.close();

}



/*
vector<bool> Parser::getASnpsSequenceFromFile(ifstream& aFileStream, size_t sizeOfSequence){
	std::vector<bool> SNPSequence;

	for (size_t i = 0 ; i < sizeOfSequence ; ++i){
		if (nextZeroOrOne(aFileStream)) {
			SNPSequence.push_back(true);
		}else{
			SNPSequence.push_back(false);
		}
	}
	return SNPSequence;
}*/

/*
 *
 */
/*
ARGnode getASequenceFromFile(std::ifstream& fluxFichier, int sizeOfSequence){
	// TODO wrong stuff here ned to check this out
	int Id = nextInt(fluxFichier);
	bool TIMGenotype = nextZeroOrOne(fluxFichier);
	bool isACase = nextZeroOrOne(fluxFichier);
	string snipsSequenceString= "";
	//SnipSequence aSequence(Id,TIMGenotype,isACase);
	for (int i = 0 ; i < sizeOfSequence ; ++i){
		if(nextZeroOrOne(fluxFichier)){
			snipsSequenceString.push_back('1');
		}else{
			snipsSequenceString.push_back('0');
		}
	}
	SnipSequence SnipsSequence(snipsSequenceString);
	ARGnode newNode(SnipsSequence,5,ARGNodeType::LEAFSAMPLEMODE);
	return newNode;
}
 */
/*
 *
 */
/*
std::vector<bool> getAFastSequenceFromFile(std::ifstream& fluxFichier, int sizeOfSequence){
	std::vector<bool> SNPSequence;
	for (int i = 0 ; i < sizeOfSequence ; ++i){
		if (nextZeroOrOne(fluxFichier)) {
			SNPSequence.push_back(true);
		}else{
			SNPSequence.push_back(false);
		}
	}



	for (const auto i : SNPSequence){
		std::cout << i << "-";
	}


//std::cout << std::endl;
//char test;
//std::cin >> test;

	return SNPSequence;

}*/




