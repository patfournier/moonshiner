/*
 * Types.h
 *
 *  Created on: 2017-06-13
 *      Author: Patrick Fournier
 */

#pragma once

#include <array>
#include <cstddef>
#include <map>
#include <vector>

#include "Helpers.h"

template <class Node>
class Types {
public:
    /***** CoalTree *****/
    // Arrays.
    typedef std::array<Node*, 2> nodes_pair;
    typedef std::vector<Node*> coalTree_ptrs;

    // Lists.
    typedef std::forward_list<Node*> listNodes_ptr;

    // Maps.
    typedef std::map<size_t, nodes_pair, std::greater<size_t>> nodesPairs_map;

    typedef std::map<size_t, Node*, std::greater<size_t>> nodes_map;

    typedef std::map<size_t, std::array<size_t, 2>, std::greater<size_t>>
        status_map;
};
