/*
 * popParameters.h
 *
 *  Created on: 2018-03-15
 *      Author: Patrick Fournier
 *
 *  This class contains attributes used to store population parameters
 *  such as mutation and recombination rate.
 */

#ifndef POPPARAMETERS_H_
#define POPPARAMETERS_H_

#include "Parameters.h"

class PopParameters {
public:
    /***** Public attributes *****/
    // 4 * Ne * mu.
    const double theta;

    // 4 * Ne * r.
    const double rho;

protected:
    /***** Private methods *****/
    // Constructors.
    PopParameters(const Parameters& params)
        : theta(4.0 * (double)params.populationEffectiveSize
                * params.mutationRate)
        , rho(4.0 * (double)params.populationEffectiveSize
              * params.recombinationRate){};
};

#endif /*POPPARAMETERS_H_*/
