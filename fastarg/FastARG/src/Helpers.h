/*
 * Helpers.h
 *
 *  Created on: 2018-05-21
 *      Author: Patrick Fournier
 *
 *  Stuff I wrote because I'm lazy.
 */

#ifndef HELPERS_H_
#define HELPERS_H_

#include <algorithm>
#include <cstdlib>
#include <forward_list>
#include <map>

/***** Simplified functions from algorithm. *****/
namespace helpers
{
template <class Iterable, class UnaryPredicate>
bool any_of(Iterable obj, UnaryPredicate p) {
    return std::any_of(obj.begin(), obj.end(), p);
};

template <class Iterable>
bool all(Iterable obj) {
    return std::all_of(
        obj.begin(), obj.end(), [](bool x) -> bool { return x; });
};

// clang-format off
template <class Iterable, class UnaryPredicate>
void remove_if(Iterable& obj, UnaryPredicate p) {
    std::remove_if(obj.begin(), obj.end(), p);
};
// clang-format on

template <class Iterable, class URBG>
void shuffle(Iterable obj, URBG&& g) {
    std::shuffle(obj.begin(), obj.end(), g);
}

/***** Fonctions on forward lists *****/
template <class T, class Generator>
void emplace_at_random(std::forward_list<T>& list,
                       T newElement,
                       size_t length,
                       Generator g) {
    size_t shift(std::uniform_int_distribution<size_t>(0, length)(g));
    auto idx = list.before_begin();
    for (size_t i = 0; i < shift; ++i) ++idx;
    list.emplace_after(idx, newElement);
}
}  // namespace helpers

#endif /*HELPERS_H_*/
