/*
 * SequentialSnipSequence.h
 *
 *  Created on: 2018-03-17
 *      Author: Patrick Fournier
 *
 *  This class represents an haplotype used for sequential ARG building.
 */

#pragma once

#include "baseSnipSequence.h"

class SequentialSnipSequence : public baseSnipSequence {
public:
    /***** Public methods *****/
    /* Constructors */
    SequentialSnipSequence(valarray<DATA_TYPE> snipsDataSequence,
                           size_t sizeOfSequence)
        : baseSnipSequence(snipsDataSequence, sizeOfSequence){};

    SequentialSnipSequence(const SequentialSnipSequence& other)
        : baseSnipSequence{other} {};

    SequentialSnipSequence(SequentialSnipSequence&& other)
        : baseSnipSequence{} {
        using std::swap;

        snipsData.swap(other.snipsData);
        swap(sizeOfSequence, other.sizeOfSequence);
        swap(numberOfBlock, other.numberOfBlock);
    };

    SequentialSnipSequence(std::string sequence)
        : baseSnipSequence{sequence} {};

    SequentialSnipSequence() = default;

    // Assigment operator.
    SequentialSnipSequence& operator=(const SequentialSnipSequence& rhs) {
        snipsData      = rhs.snipsData;
        sizeOfSequence = rhs.sizeOfSequence;
        numberOfBlock  = rhs.numberOfBlock;

        return *this;
    };

    // Move assigment operator.
    SequentialSnipSequence& operator=(SequentialSnipSequence&& rhs) {
        using std::swap;

        if (&rhs != this) {
            snipsData.swap(rhs.snipsData);
            swap(sizeOfSequence, rhs.sizeOfSequence);
            swap(numberOfBlock, rhs.numberOfBlock);
        }

        return *this;
    };

    // Overloading of &= operator.
    SequentialSnipSequence& operator&=(const SequentialSnipSequence& rhs) {
        snipsData &= rhs.snipsData;
        return *this;
    };

    // Overloading of & operator.
    friend SequentialSnipSequence operator&(SequentialSnipSequence lhs,
                                            const SequentialSnipSequence& rhs) {
        lhs &= rhs;
        return lhs;
    };

    // Overloading of ^= operator.
    SequentialSnipSequence& operator^=(const SequentialSnipSequence& other) {
        snipsData ^= other.snipsData;
        return *this;
    };

    // Overloading of ^ operator.
    friend SequentialSnipSequence operator^(SequentialSnipSequence lhs,
                                            const SequentialSnipSequence& rhs) {
        lhs ^= rhs;
        return lhs;
    };

    // True if the sequence contains only 0.
    bool all0() const {
        using std::begin;
        using std::end;

        for (auto n{end(snipsData) - 1}; n >= begin(snipsData); --n)
            if (*n > 0) return false;
        return true;
    };

    // Checks if a position has the derived allele.
    bool operator[](size_t thePosition) const {
        return snipsData[(thePosition / BIT_SIZE_OF_DATA_TYPE)]
               & (BIT_INDICATOR >> (thePosition % BIT_SIZE_OF_DATA_TYPE));
    }
};
