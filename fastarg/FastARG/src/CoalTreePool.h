/*
 * CoalTreePool.h
 *
 *  Created on: 2017-03-27
 *      Author: Patrick Fournier
 *
 *  This class manages the storage of a bunch CoalTree representing an ARG.
 */

#ifndef COALTREEPOOL_H_
#define COALTREEPOOL_H_

#include "ARGnode.h"
#include "CoalTree.h"
#include "SequentialARG.h"

class CoalTreePool {
public:
    /***** Constructors and destructors *****/

    // Constructors.
    CoalTreePool(const std::vector<SequentialSnipSequence>& haplotypes,
                 const std::vector<bool>& phenotypes,
                 size_t nbMarkers,
                 class MoonShine* arg)
        : _nbLeaves(haplotypes.size())
        , _nbMarkers{nbMarkers} {
        assert(haplotypes.size() > 0);
        size_t nbToReserve{_nbLeaves + _nbMarkers * (_nbLeaves - 1)};

        _pool = static_cast<CoalTree*>(
            std::malloc(nbToReserve * sizeof(CoalTree)));

        if (!_pool) throw std::bad_alloc();

        // Initializes ARG by constructing leaves.
        for (size_t i = 0; i < _nbLeaves; ++i)
            new (_pool + i) CoalTree(haplotypes[i], phenotypes[i], arg);

        _currentFreeNode = _pool + _nbLeaves;
    };

    // Move constructor.
    CoalTreePool(CoalTreePool&& other)
        : _nbLeaves{other._nbLeaves}
        , _nbMarkers{other._nbMarkers} {
        _pool                  = other._pool;
        other._pool            = nullptr;
        _currentFreeNode       = other._currentFreeNode;
        other._currentFreeNode = nullptr;
    };

    // Copy constructor.
    CoalTreePool(const CoalTreePool& other)
        : _nbLeaves{other._nbLeaves}
        , _nbMarkers{other._nbMarkers} {
        size_t nbToReserve{_nbLeaves + _nbMarkers * (_nbLeaves - 1)};

        _pool = static_cast<CoalTree*>(
            std::malloc(nbToReserve * sizeof(CoalTree)));
        if (!_pool) throw std::bad_alloc();
        _currentFreeNode = _pool;

        // Copies nodes from other CoalTreePool.
        for (CoalTree* node{other._pool}; node < other._currentFreeNode;
             ++node) {
            new (_currentFreeNode++) CoalTree{*node};
        }
    };

    CoalTreePool()
        : _pool{nullptr}
        , _currentFreeNode{nullptr}
        , _nbLeaves{0} {};

    // Destructor
    ~CoalTreePool() {
        for (CoalTree* node{_pool}; node < _currentFreeNode; ++node)
            node->~CoalTree();
        std::free(this->_pool);
    };

    // Update the arg pointer of stored nodes.
    void update_arg(class MoonShine* arg) {
        if (!_pool) return;
        for (auto node{begin()}; node <= _currentFreeNode; ++node)
            node->update_arg(arg);
    };

    // Move assigment operator.
    CoalTreePool& operator=(CoalTreePool&& other) {
        _nbLeaves = other._nbLeaves;

        _pool       = other._pool;
        other._pool = nullptr;

        _currentFreeNode       = other._currentFreeNode;
        other._currentFreeNode = nullptr;

        return *this;
    };

    // Copy assigment operator.
    CoalTreePool& operator=(const CoalTreePool& other) {
        CoalTreePool ret{other};

        _nbLeaves  = other._nbLeaves;
        _nbMarkers = other._nbMarkers;

        size_t nbToReserve{_nbLeaves + _nbMarkers * (_nbLeaves - 1)};

        _pool = static_cast<CoalTree*>(
            std::malloc(nbToReserve * sizeof(CoalTree)));
        if (!_pool) throw std::bad_alloc();
        _currentFreeNode = _pool;

        // Copies nodes from other CoalTreePool.
        for (CoalTree* node{other._pool}; node < other._currentFreeNode;
             ++node) {
            new (_currentFreeNode++) CoalTree{*node};
        }

        return *this;
    };

    /***** Iteration *****/

    // Iterators on leaves.
    CoalTree* begin() { return _pool; };
    CoalTree* begin() const { return _pool; };

    CoalTree* end() { return _pool + _nbLeaves; };
    CoalTree* end() const { return _pool; }

    /***** Accessors *****/
    // Returns the number of nodes in the ARG.
    size_t nbNodes() const { return _currentFreeNode - _pool; };

    /***** Modifier methods *****/
    // Puts a coalescence node in the current free node and returns a pointer on
    // the newly created node.
    CoalTree* emplace_back(std::array<CoalTree*, 2>&& children,
                           size_t nbLiveNodes,
                           const SequentialARG& parentARG,
                           mt19937& eng) {
        using std::forward;

        new (_currentFreeNode)
            CoalTree(forward<std::array<CoalTree*, 2>>(children),
                     nbLiveNodes,
                     parentARG,
                     eng);

        return _currentFreeNode++;
    };

    // Puts a recoalescence node in the current free node and returns a pointer
    // on the newly created node.
    CoalTree* emplace_back(size_t pos,
                           CoalTree& child,
                           double recombinationHeight,
                           double trunc,
                           size_t& leftmost,
                           const std::vector<size_t>& snpCoordinates,
                           mt19937& eng) {
        new (_currentFreeNode) CoalTree(pos,
                                        child,
                                        recombinationHeight,
                                        trunc,
                                        leftmost,
                                        snpCoordinates,
                                        eng);

        return _currentFreeNode++;
    };

protected:
    // Actual heap storage.
    CoalTree* _pool{nullptr};

    // Current free node.
    CoalTree* _currentFreeNode{nullptr};

    // Number of leaves.
    size_t _nbLeaves;

    // number of markers by sequence.
    size_t _nbMarkers;
};

#endif /*COALTREEPOOL_H_*/
