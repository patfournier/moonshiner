/*
 * CoalTree.h
 *
 *  Created on: 2018-03-09
 *      Author: Patrick Fournier
 */

#ifndef COALTREE_H_
#define COALTREE_H_

// #include <boost/progress.hpp>
// #include <set>
#include <algorithm>
#include <array>
#include <cassert>
#include <map>

#include "Children.h"
#include "Haplotypes.h"
#include "Helpers.h"
#include "SequentialARG.h"
#include "SequentialSnipSequence.h"
#include "Types.h"

class CoalTree : protected Types<CoalTree> {
public:
    /***** Types *****/
    typedef typename Types<CoalTree>::nodes_map nodes_map;

    /***** Public methods *****/
    /* Constructors */

    // Constructor for a leaf.
    CoalTree(const SequentialSnipSequence& sequence,
             bool control,
             class MoonShine* arg)
        : arg{arg}
        , haplotypes{0, sequence}
        , height{0.0}
        , descendantStatus{
              control
                  ? status_map{std::make_pair(0, std::array<size_t, 2>{1, 0})}
                  : status_map{
                        std::make_pair(0, std::array<size_t, 2>{0, 1})}} {};

    // Constructor for a coalescence node (in T0).
    CoalTree(nodes_pair&& children,
             size_t nbLiveNodes,
             const SequentialARG& parentARG,
             mt19937& eng);

    // Constructor for a recoalescence node.
    CoalTree(size_t pos,
             CoalTree& child,
             double recombinationHeight,
             double trunc,
             size_t& leftLimit,
             const std::vector<size_t>& snpCoordinates,
             mt19937& eng);

    // Copy constructor.
    CoalTree(const CoalTree& other)
        : arg{other.arg}
        , haplotypes{other.haplotypes}
        , parentCoalTree{copy_parentCoalTree(other)}
        , children{copy_children(other)}
        , height{other.height}
        , descendantStatus{other.descendantStatus} {};

    // Move constructor.
    CoalTree(CoalTree&& other)
        : arg{other.arg}
        , haplotypes{std::move(other.haplotypes)}
        , parentCoalTree{std::move(other.parentCoalTree)}
        , children{std::move(other.children)}
        , height{other.height}
        , descendantStatus{std::move(other.descendantStatus)} {
        std::cout << "move" << std::endl;
    };

    CoalTree() = default;

    CoalTree& operator=(const CoalTree& other) {
        arg              = other.arg;
        haplotypes       = other.haplotypes;
        parentCoalTree   = copy_parentCoalTree(other);
        children         = copy_children(other);
        height           = other.height;
        descendantStatus = other.descendantStatus;

        return *this;
    }

    /* Methods */
    // Checks if a position in mmnAnd is mutant.
    bool operator[](size_t pos) const { return haplotypes[pos][pos]; };

    bool isPositionMutant(size_t pos) const { return operator[](pos); };

    // Being live is equivalent to being the root of a tree.
    bool isLive(size_t pos = 0) { return isMrca(pos); };

    // Bottom-up recursive computation of the value  of mmnXor.
    void compute_mmnXor(
        std::vector<std::pair<CoalTree*, SequentialSnipSequence>>& mmnXors,
        size_t pos,
        const SequentialSnipSequence& leftMask);

    // Find the new derivedEdge after a recombination.
    CoalTree* newDerivedEdge(std::vector<CoalTree*>& toRemove, size_t pos);

    // Used to determine the minimal number of mutations.
    // std::pair<CoalTree*, double> findMutationEdge(size_t pos);

    // Returns the brother of a node.
    CoalTree* brother(size_t pos) const {
        if (dad(pos)) {
            for (CoalTree* child : dad(pos)->children[pos])
                if (child != this) return child;
        }
        return nullptr;
    };

    // Returns the father of a node.
    CoalTree* dad(size_t pos) const {
        return parentCoalTree.lower_bound(pos)->second;
    };

    // Returns the uncle of a node.
    CoalTree* uncle(size_t pos) const { return dad(pos)->brother(pos); };

    // Returns the grandfather of a node.
    CoalTree* grandad(size_t pos) const {
        return isMrca(pos) ? nullptr : dad(pos)->dad(pos);
    };

    // Returns the weight of a node.
    double weight(size_t pos,
                  CoalTree* limitEdge,
                  const std::array<double, 2>& mDR) const;

    // Generate a recombination point.
    std::pair<CoalTree*, double> generateRecombination(
        size_t pos,
        CoalTree* deWithHighestDad,
        const std::array<double, 2>& mDR,
        mt19937& eng);

    // Finds nodes with a parental edge passing at a certain height and return
    // the leftmost position at which they are compatible with *this.
    void findPossibleRecoalescences(
        std::vector<CoalTree*>& possibleRecoalescences,
        const size_t pos,
        CoalTree& candidate,
        const double recHeight) const;

    // Makes *this recombine and recoalesce. The function returns 1) the brother
    // of the recombinant node, 2) the (discretized) position of the
    // recombination, 3) the height of the recombination and 4) the height of
    // the recoalescence. If no recoalescence is possible, the first member will
    // be the null pointer.
    std::tuple<const CoalTree*, size_t, double, double> recRec(
        size_t rightmost,
        size_t leftmost,
        double recPosOnBranchs,
        std::array<double, 2> mDR);

    // Compute the height of the two highest dads and finds the node with the
    // highest dad.
    void highestDads(std::array<double, 2>& mDR,
                     CoalTree*& deWithHighestDad,
                     size_t pos);

    // Reset *this mmn related attributes.
    void mmnReset() { mmnVisited = false; };

    // Bottom-up update of haplotypes.
    void update_haplotype(size_t pos) {
        using std::get;
        using std::move;

        const SequentialSnipSequence& oldSequence{haplotypes[pos]};

        const SequentialSnipSequence& newSequence{
            get<0>(children[pos])->haplotypes[pos]
            & get<1>(children[pos])->haplotypes[pos]};

        if (newSequence == oldSequence) return;

        haplotypes.insert_or_assign(pos, move(newSequence));

        if (dad(pos)) dad(pos)->update_haplotype(pos);
    };

    // Bottom-up update of descendantStatus.
    void update_descendantStatus(size_t pos) {
        using std::get;
        using std::make_pair;

        std::array<size_t, 2> val{
            initialize_descendantStatus(pos, children[pos])[pos]};

        descendantStatus.insert_or_assign(pos, val);

        if (!parentCoalTree.empty()) dad(pos)->update_descendantStatus(pos);
    };

    // Get the leaves that are the descendants of *this.
    void getLeaves(std::vector<CoalTree*>& leaves, size_t pos) {
        using std::get;

        if (isLeaf())
            leaves.emplace_back(this);
        else
            for (CoalTree* child : children[pos]) child->getLeaves(leaves, pos);
    };

    // Returns the first and last tree for which *this is ancestral.
    size_t firstTree() {
        auto end{parentCoalTree.end()};
        --end;
        return end->first;
    };

    size_t lastTree() { return parentCoalTree.begin()->first; };

    // Updates the arg pointer.
    void update_arg(class MoonShine* arg) { this->arg = arg; };

    // Returns the number of nodes at a strictly lower lattitude than *this.
    static size_t nb_liveNodes(MoonShine* arg, double height);

    // Returns true if *this is the gmrca.
    bool isGmrca() const;

    /* Probabilities */
    // Probability of the phenotype vector.
    void prob_pheno(std::vector<std::array<double, 2>>& probs,
                    size_t pos,
                    const std::array<size_t, 2>& totalPheno,
                    const std::array<double, 2>& penetrance) {
        using std::get;

        for (CoalTree* child : children[pos])
            child->prob_pheno_helper(probs, pos, totalPheno, penetrance);
    };

    void prob_pheno_helper(std::vector<std::array<double, 2>>& probs,
                           size_t pos,
                           const std::array<size_t, 2>& totalPheno,
                           const std::array<double, 2>& penetrance);

    long double prob_mutation(size_t pos,
                              const std::vector<size_t>& snpsCoordinates,
                              mt19937& eng,
                              bool log = true);

    /***** Debuging *****/
    static void output_tree_helper(CoalTree& node,
                                   size_t pos,
                                   size_t idLength,
                                   size_t mult,
                                   size_t offset);

    // Tests the coherence of a marginal tree.
    void tree_coherent(size_t pos, size_t& count);

protected:
    /***** Protected attributes *****/

    class MoonShine* arg;

    // If *this is a leaf, contains a sampled sequence. Otherwise, contains the
    // conjunction of the haplotypes of the children of *this.
    Haplotypes haplotypes;

    // Parent nodes and the leftmost position for which it is *this parent.
    nodes_map parentCoalTree;

    // Children of the node. If the right entry is NULL, this is a mutation
    // node. If both entries are NULL, this is a leaf.
    Children children;

    // Height of the node.
    double height;

    // Exclusive disjunction (xor) of this and parent's mmnAnd.
    bool mmnVisited{false};

    // Number of descendant cases and controls.
    status_map descendantStatus;

    /***** Protected methods *****/
    /* Helper methods for constructors. */

    // Returns the union of the haplotypes vector from the two members of
    // the pair.
    vector<SequentialSnipSequence*> computeUnionHaplotypes(
        const std::array<CoalTree*, 2>& children);

    // Compute the height of the node in T0.
    double computeHeight(const std::array<CoalTree*, 2>& children,
                         size_t nbLiveNodes,
                         const SequentialARG& parentARG,
                         mt19937& eng);

    status_map initialize_descendantStatus(size_t pos,
                                           const Children::children& children) {
        using std::get;
        using std::make_pair;

        const std::array<size_t, 2>& child1Status{
            get<0>(children)->descendantStatus.lower_bound(pos)->second};
        const std::array<size_t, 2>& child2Status{
            get<1>(children)->descendantStatus.lower_bound(pos)->second};

        std::array<size_t, 2> ret{get<0>(child1Status) + get<0>(child2Status),
                                  get<1>(child1Status) + get<1>(child2Status)};

        return {make_pair(pos, ret)};
    };

    nodes_map copy_parentCoalTree(const CoalTree& other);

    Children copy_children(const CoalTree& other);

    /* Other protected methods */

    // Determines if this is a root.
    bool isMrca(size_t pos) const;

    // Determines if this is a leaf.
    bool isLeaf() const { return children.size() == 0; };

    // Finds the maximum recoalescence height.
    double maxRecoalescenceHeight(size_t pos);

    // Determines the leftmost limit for a recombination.
    void leftLimit_helper(size_t pos, size_t leftmost, size_t& leftLimit);

    size_t leftLimit(size_t pos, size_t leftmost) {
        size_t leftLimit;

        leftLimit_helper(pos, leftmost, leftLimit);

        return leftLimit;
    };
};

#endif /*COALTREE_H_*/
