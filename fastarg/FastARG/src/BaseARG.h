/*
 * baseARG.h
 *
 *  Created on: 2018-03-15
 *      Author: Éric Marcotte & Patrick Fournier
 *
 *  This virtual class contains attributes and methods shared
 *  among every ARG classes.
 */

#ifndef BASEARG_H_
#define BASEARG_H_

#include <chrono>
#include <random>
#include <set>

#include "Enums.h"
#include "Parameters.h"

using std::mt19937;
using std::string;
using std::vector;

class BaseARG {
public:
    /***** Public attributes *****/
    // Flag for how recombination events must be performed.
    recombinationMode recombinationModeFlag;

    // Basic attributes of sequences of the ARG.
    size_t lenghtOfSequences;
    size_t numberOfSequences;

    // Core data attributes.
    vector<bool> phenotypes;
    vector<size_t> SnipsCoordinates;

    /***** Public methods *****/
    // Constructors.
    BaseARG(const Parameters& params);

    // Destructor.
    virtual ~BaseARG(){};

    // Pure virtual function that makes the class abstract.
    virtual void run() = 0;

    // Returns the number of leaves in the ARG.
    size_t nbLeaves() const { return numberOfSequences; };

    // Service methods needed by FastARG.
    virtual void printARGSummary() = 0;
    virtual vector<uint16_t> getLeafReachabilityforALocus(
        size_t theLocusIndex,
        size_t nodeIndex) const                              = 0;
    virtual vector<long double> chi2AssociationTestResults() = 0;
};

#endif /*BASEARG_H_*/
