/*
 * baseARG.cpp
 *
 *  Created on: 2018-03-15
 *      Author: Éric Marcotte & Patrick Fournier
 *
 *  This virtual class contains attributes and methods shared
 *  among every ARG classes.
 */

#include "BaseARG.h"

BaseARG::BaseARG(const Parameters& params)
    : recombinationModeFlag{params.recombinationModeChoice}
    , lenghtOfSequences{params.sequencesSize}
    , numberOfSequences{params.numberOfSequences}
    , phenotypes{params.phenotypes}
    , SnipsCoordinates{params.locusCoordinates} {};
