/*
 * ARGraph.h
 *
 *  Created on: 2015-09-05
 *      Author: �ric Marcotte
 */

#ifndef ARG_H_
#define ARG_H_

#include "ARGnode.h"
#include "BaseARG.h"
#include "SnipSequence.h"

typedef std::chrono::high_resolution_clock Clock;

// Abstract base class for an ARG.
class ARG : public BaseARG {
public:
    // Basic attributes of sequences of the ARG.
    size_t maxGenerationSize;

    // Number of events counters.
    size_t totalNumberOfMutationEvent;
    size_t totalNumberOfCoalescenceEvent;
    size_t totalNumberOfRecombinationEvent;

    // Metrics on the current state of the ARG.
    size_t numberOfCoalescencePossible;
    size_t numberOfMutationPossible;
    size_t numberOfPositionToBeMuted;

    // Used while constructing an ARG.
    bool isAMutationEventPossible;

    // Core data attributes.
    vector<size_t> numberOfMutantSNPsByPosition;
    std::set<std::pair<size_t, size_t>> possibleCoalescence;
    vector<size_t> positionOfAliveNodes;
    vector<ARGnode> ARGnodes;

    // Auxiliary seeds and RNG.
    mt19937 eng;
    size_t seed;

    // Constructors
    ARG(Parameters params,
        vector<ARGnode> Leaves,
        const vector<bool>& phenotypes,
        const vector<size_t>& coordinates);

    // Destructors.
    // TODO Check for rule of 0,3,5.
    virtual ~ARG() = default;

    // Principal functions.
    virtual void generateACoalescentEvent(size_t firstSourceNode,
                                          size_t secondSourceNode);
    virtual void generateARecombinationEvent(size_t indexOfTheRecombination,
                                             size_t sourceNode);
    virtual void generetaAMutationEvent(size_t positionOfTheMutation,
                                        size_t sourceNode);

    // Random generators functions.
    bool generateARandomCoalescentEvent();
    bool generateARandomMutationEvent();
    bool generateARandomRecombinationEvent();

    // Services on the graph and the data.
    void printLivesNodes();
    virtual void printARGSummary();
    void printCoalescencePairs();
    void dumpARGDataInFile(string aFileName);

    vector<uint16_t> getLeafReachabilityforALocus(size_t theLocusIndex,
                                                  size_t nodeIndex) const;
    string getPartialTreeForALocusInNewick(size_t theLocusIndex);
    string getPartialTreeForALocusInNewick(size_t theLocusIndex,
                                           size_t nodeIndex);

    // TODO functions that resets the ARG data structure.
    // virtual void reset(void);

    // Statistics functions.
    long double chi2TestForASetOfLeavesAndAnLocus(vector<uint16_t> theLeaves,
                                                  size_t aLocus);
    // long double chiSquareOnDataOnlyForAPosition( size_t );
    vector<long double> chi2AssociationTestResults();
    vector<long double> MaxChi2TestForTheARG();
    // vector<long double> AvgChi2PValuesTestForTheARG();
    // vector<long double> AvgUpperQuartileChi2PValuesTestForTheARG();
};

#endif /* ARG_H_ */
